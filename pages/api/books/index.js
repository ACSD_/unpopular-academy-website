import connect from "next-connect";
import { books } from "../../../source/database";

const handler = connect();

handler.get(async (req, res) => {
    const catalog = await books.catalog(req.query);

    res.json(catalog);
});

handler.post((req, res) => {
    books
        .create(req.body)
        .then((book) => res.json(book))
        .catch((reason) => {
            console.log(reason);
            res.statusCode = 400;
            res.json({});
        });
});

export default handler;
