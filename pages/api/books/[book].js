import connect from "next-connect";
import { books } from "../../../source/database";

const handler = connect();

handler.get(async (req, res) => {
    const {
        query: { book },
    } = req;

    res.json({});
});

export default handler;
