import useSWR from "swr";
import { useRouter } from "next/router";
import AddBookController from "../../source/components/AddBookController";
import BookPreview from "../../source/components/BookPreview";
import PagedGrid from "../../source/components/PagedGrid";

const fetcher = (url) => fetch(url).then((r) => r.json());

export default function BooksPage(props) {
    const router = useRouter();
    const { data, error } = useSWR(`/api${router.asPath}`, fetcher);

    if (error) return <div style={{ height: "90vh" }}>failed to load</div>;
    if (!data) return <div style={{ height: "90vh" }}>loading...</div>;

    return (
        <div>
            <AddBookController />
            <PagedGrid ipp={18}>
                {data.map((book) => (
                    <BookPreview {...book} />
                ))}
            </PagedGrid>
        </div>
    );
}
