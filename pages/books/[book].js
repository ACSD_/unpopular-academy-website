import { useRouter } from "next/router";

export default function BooksPage() {
    const router = useRouter();
    const { book } = router.query;

    return (
        <>
            <div style={{ height: "90vh" }}>{book} Placeholder</div>
        </>
    );
}
