import Head from "next/head";

function HomePage() {
    return (
        <>
            <Head>
                <title>Unpopular Academy Videos</title>
            </Head>
            <div style={{height: "90vh"}}>
                Videos Page Placeholder
            </div>
        </>
    );
}

export default HomePage;
