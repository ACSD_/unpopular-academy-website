import Head from "next/head";

function HomePage() {
    return (
        <>
            <Head>
                <title>Unpopular Academy</title>
            </Head>
            <div style={{height: "90vh"}}>
                Home Page Placeholder
            </div>
        </>
    );
}

export default HomePage;
