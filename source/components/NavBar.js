import { Nav, Navbar, Form, FormControl, Button } from "react-bootstrap";

const navBarStyle = {
    backgroundColor: "red",
    color: "white",
    width: "100%",
    height: "60px",
};

const NavBar = () => {
    return (
        <Navbar bg="dark" variant="dark" style={{ height: "10vh" }}>
            <Navbar.Brand href="/">Unpopular Academy</Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link href="books">Books</Nav.Link>
                <Nav.Link href="videos">Videos</Nav.Link>
                <Nav.Link href="people">People</Nav.Link>
            </Nav>
            <Form inline method="GET">
                <FormControl
                    name="title"
                    type="text"
                    placeholder="Search"
                    className="mr-sm-2"
                />
                <Button variant="outline-info" type="submit">Search</Button>
            </Form>
        </Navbar>
    );
};

export default NavBar;
