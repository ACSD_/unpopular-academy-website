import { useState } from "react";
import { Col, Container, Pagination, Row } from "react-bootstrap";
import inbatches from "../util/batches";

function ElementGrid(props) {
    const batches = inbatches(props.children, 3);
    return (
        <Container>
            {batches.map((batch) => (
                <Row>
                    {batch.map((Item) => (
                        <Col style={{ minHeight: "300px" }}>{Item}</Col>
                    ))}
                </Row>
            ))}
        </Container>
    );
}

function SmartPagination({ count, selected, ...props }) {
    let [page, setPage] = useState(0);
    const click = (idx) => () => (setPage(idx), selected && selected(idx));

    return (
        <Pagination {...props}>
            {[...Array(count)].map((v, i) => (
                <Pagination.Item key={i} active={i == page} onClick={click(i)}>
                    {i + 1}
                </Pagination.Item>
            ))}
        </Pagination>
    );
}

function PagedGrid({ ipp, children, ...props }) {
    const page_count = Math.ceil(children.length / ipp);
    let [page, setPage] = useState(0);

    return (
        <Container>
            <Row style={{ paddingTop: "20px" }}>
                <ElementGrid>
                    {children.slice(page * 18, (page + 1) * 18)}
                </ElementGrid>
            </Row>
            <Row className="justify-content-md-center">
                <SmartPagination
                    count={page_count}
                    selected={setPage}
                    size="lg"
                />
            </Row>
        </Container>
    );
}

export default PagedGrid;
