import { Card } from "react-bootstrap";

function BookPreview(props) {
    const { idx, title, author, yop } = props;
    return (
        <Card
            bg="dark"
            key={idx}
            text={"white"}
            style={{ width: "18rem" }}
            href="test"
        >
            <Card.Header>{title}</Card.Header>
            <Card.Img variant="top" />
            <Card.Body>
                <Card.Title>Author: {author || "N/A"}</Card.Title>
                <Card.Text>Year of Publication: {yop || "N/A"}</Card.Text>
            </Card.Body>
        </Card>
    );
}

export default BookPreview;