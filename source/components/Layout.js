import Footer from "./Footer";
import NavBar from "./NavBar";


const Layout = (props) => (
    <div className="Layout">
        <NavBar />
        <div className="Content">
            {props.children}
        </div>
        <Footer />
    </div>
);

export default Layout;
