import { Formik } from "formik";
import { useState } from "react";
import { Button, Col, Form, Modal } from "react-bootstrap";
import * as yup from "yup";

import { validYear } from "../util/validators";

const schema = yup.object({
    title: yup.string().required(),
    author: yup.string().required(),
    yop: yup
        .string()
        .test("year of publication", " is not a valid year [WIP]", validYear)
        .required(),
    edition: yup.string().required(),
    publisher: yup.string().required(),
    isbn: yup.string(),
    isbn10: yup.string(),
    isbn13: yup.string(),
    description: yup.string(),
    plot: yup.string(),
});

function fuzz() {
    return new Promise((resolve) => setTimeout(resolve, 2000));
}

//prettier-ignore
function FGroup({ md, label, feedback, ...control }) {
    return (
        <Form.Group as={Col} md={md} controlId={`validate_${control.name}`}>
            <Form.Label>{label}</Form.Label>
            <Form.Control {...control} />
            <Form.Control.Feedback type={control.isInvalid ? "invalid" : "valid"} >
                {feedback!=null?label+" "+feedback.substr(feedback.indexOf(" "), feedback.lengh):null}
            </Form.Control.Feedback>
        </Form.Group>
    );
}

const json = (response) => response.json();

function AddBookModal(props) {
    function postData(data, methods) {
        fetch("/api/books", {
            method: "post",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
            },
            body: JSON.stringify(data),
        })
            .then(json)
            .then((response) => {
                console.log("RES", response);
                console.log(methods);
                props.onHide();
            })
            .catch((reason) => {
                console.warn(reason);
            });
    }

    return (
        <Modal {...props}>
            <Formik
                validationSchema={schema}
                onSubmit={postData}
                initialValues={{}}
            >
                {({
                    handleSubmit,
                    handleChange,
                    handleBlur,
                    values,
                    touched,
                    isValid,
                    errors,
                }) => (
                    <Form noValidate onSubmit={handleSubmit}>
                        <Modal.Header>
                            <Modal.Title>Modal heading</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form.Row>
                                <FGroup
                                    md="6"
                                    label={"Book Title"}
                                    type="text"
                                    name="title"
                                    placeholder="Book Title"
                                    value={values.title}
                                    onChange={handleChange}
                                    isInvalid={!!errors.title}
                                    feedback={errors.title}
                                />

                                <FGroup
                                    md="6"
                                    label={"Author"}
                                    type="text"
                                    name="author"
                                    placeholder="Author"
                                    value={values.author}
                                    onChange={handleChange}
                                    isInvalid={!!errors.author}
                                    feedback={errors.author}
                                />
                            </Form.Row>
                            <Form.Row>
                                <FGroup
                                    md="8"
                                    label={"Edition"}
                                    type="text"
                                    name="edition"
                                    placeholder="Edition"
                                    value={values.edition}
                                    onChange={handleChange}
                                    isInvalid={!!errors.edition}
                                    feedback={errors.edition}
                                />
                                <FGroup
                                    md="4"
                                    label={"Year of Publication"}
                                    type="text"
                                    name="yop"
                                    placeholder="YYYY"
                                    value={values.yop}
                                    max={new Date().getFullYear()}
                                    onChange={handleChange}
                                    isInvalid={!!errors.yop}
                                    feedback={errors.yop}
                                />
                            </Form.Row>
                            <Form.Row>
                                <FGroup
                                    md="4"
                                    label={"ISBN"}
                                    type="text"
                                    name="isbn"
                                    placeholder="ISBN"
                                    value={values.isbn}
                                    onChange={handleChange}
                                    isInvalid={!!errors.isbn}
                                    feedback={errors.isbn}
                                />
                                <FGroup
                                    md="4"
                                    label={"ISBN-10"}
                                    type="text"
                                    name="isbn10"
                                    placeholder="ISBN-10"
                                    value={values.isbn10}
                                    onChange={handleChange}
                                    isInvalid={!!errors.isbn10}
                                    feedback={errors.isbn10}
                                />
                                <FGroup
                                    md="4"
                                    label={"ISBN-13"}
                                    type="text"
                                    name="isbn13"
                                    placeholder="ISBN-13"
                                    value={values.isbn13}
                                    onChange={handleChange}
                                    isInvalid={!!errors.isbn13}
                                    feedback={errors.isbn13}
                                />
                            </Form.Row>
                            <Form.Row>
                                <FGroup
                                    md="8"
                                    label={"Publisher"}
                                    type="text"
                                    name="publisher"
                                    placeholder="Publisher"
                                    value={values.publisher}
                                    onChange={handleChange}
                                    isInvalid={!!errors.publisher}
                                    feedback={errors.publisher}
                                />
                            </Form.Row>

                            <Form.Group controlId="validate_description">
                                <Form.Label>Description</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    name="description"
                                    onChange={handleChange}
                                />
                            </Form.Group>

                            <Form.Group controlId="validate_plot">
                                <Form.Label>Plot</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    name="plot"
                                    onChange={handleChange}
                                />
                            </Form.Group>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="danger" onClick={props.onHide}>Cancel</Button>
                            <Button type="submit">Add Book</Button>
                        </Modal.Footer>
                    </Form>
                )}
            </Formik>
        </Modal>
    );
}

function AddBookButton(props) {
    return <Button {...props}>Add Book</Button>;
}

function AddBookController(props) {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <AddBookButton variant="secondary" onClick={handleShow} block />
            <AddBookModal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                centered
            />
        </>
    );
}

export default AddBookController;
