import { Schema } from "mongoose";

import model from "../util/safe-model";
import SafeAPI from "../util/safe-api";

const BookSchema = new Schema({
    title: { type: String, required: true },
    author: { type: String },
    yop: { type: String },
    edition: { type: String },
    publisher: { type: String },
    isbn: { type: String },
    isbn10: { type: String },
    isbn13: { type: String },
    description: { type: String },
    plot: { type: String },
});

BookSchema.plugin(SafeAPI);

const BookModel = model("Book", BookSchema);
export default BookModel;
