import mongoose from "mongoose";

const MONGO_URI = process.env.MONGO_URI;
if (!mongoose.connection.db) {
    console.log("MONGOOSE ENV", process.env.MONGO_URI);
    console.log("MONGOOSE RECONNECT", MONGO_URI);
    mongoose.connect(MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
}

export * as books from './books';
