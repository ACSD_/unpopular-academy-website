import Book from "../models/Book";

export async function catalog({ title }) {
    let query = {};
    if (title) {
        query.title = {
            $in: title.split(" ").map((v) => new RegExp(`.*${v}.*`)),
        };
    }

    return Book.find(query, (err, books) => {
        if (err) throw err;
        return books.map((book) => book._api());
    });
}

export async function create(data) {
    const book = new Book(data);
    await book.save();
    return book._api();
}
