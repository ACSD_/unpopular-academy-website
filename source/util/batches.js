function batches(list, size) {
    return list.reduce((p, c, i) => {
        const x = (i / size) << 0;
        if (!p[x]) p[x] = [];
        
        p[x][i % size] = c;

        return p;
    }, []);
}

export default batches;
