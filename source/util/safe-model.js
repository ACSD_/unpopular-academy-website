const mongoose = require("mongoose");

module.exports = (name, schema) =>
    mongoose.models && name in mongoose.models
        ? mongoose.model(name)
        : mongoose.model(name, schema);
