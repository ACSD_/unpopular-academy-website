//prettier-ignore
const lut = [...new Array(255)].map((_,i)=>(i < 16 ? "0" : "") + i.toString(16));
// for (var i = 0; i < 256; i++) { lut[i] = (i < 16 ? "0" : "") + i.toString(16); }

let _ = null;
const u = () =>
    Date.now().toString(16) + Math.random().toString(16) + "0".repeat(16);
const guid = () => (
    (_ = u()),
    [
        _.substr(0, 8),
        _.substr(8, 4),
        "4000-8" + _.substr(13, 3),
        _.substr(16, 12)
    ].join("-")
);

export default guid;
