//prettier-ignore
export function validYear(value) {
    const m = value.match(/^([\d]+)\s?([Aa][dD]|[Bb][Cc])?$/);
    const [_, y, t = "ad"] = m || [], Y = Number(y);
    return !!Y && t.toLowerCase() == "ad" && Y <= new Date().getFullYear();
}


//TODO validISBN(vaulue)
//TODO validISBN10(value)
//TODO validISBN13(value)