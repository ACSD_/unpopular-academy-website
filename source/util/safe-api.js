export default function SafeAPI(schema) {
    schema.methods._api = function _api() {
        const entries = Object.entries(this._doc)
            // .filter(([key])=>!key.startsWith("$"))
            .filter(([key]) => !key.startsWith("_"));
        entries.push(["id", this.id]);

        return Object.fromEntries(entries);
    };
}
